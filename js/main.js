$(document).ready(function(){
    console.log('ready')
    $('.slider').slick({
        infinite: true,
        autoplay: true,
        slidesToShow: 5,
        slidesToScroll: 1,
        centerMode: true,
        variableWidth: true
      });
});